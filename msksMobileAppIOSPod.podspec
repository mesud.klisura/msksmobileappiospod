#
# Be sure to run `pod lib lint msksMobileAppIOSPod.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'msksMobileAppIOSPod'
  s.version          = '0.2'
  s.summary          = 'Biblioteka za mobilnu aplikaciju MSKS'
  
  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  
  s.description      = <<-DESC
  'Biblioteka za mobilnu aplikaciju MSKS koja omogućava povezivanje s Monri sistemom kartičnog plaćanja.'
  DESC
  
  s.homepage         = 'https://gitlab.com/msks-jgs/frontend-mobile/-/tree/master/ios-pod'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Armin Mahmutović' => 'armin.mahmutovic@ngs.ba' }
  s.source           = { :git => 'https://gitlab.com/msks-jgs/frontend-mobile/-/tree/master/ios-pod.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.ios.deployment_target = '10.0'
  
  s.source_files = 'Source/**/*.swift'
  s.swift_version='5.0'
  s.platforms={
    "ios":"10.0"
  }
  
  # s.resource_bundles = {
  #   'podFive' => ['podFive/Assets/*.png']
  # }
  
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'Alamofire', '~> 4.0'
  s.dependency 'Monri', '~> 1.0'
end
