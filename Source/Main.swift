//
//  Main.swift
//  msksMobileAppIOSPod
//
//  Created by Armin on 23/06/2020.
//

import Foundation
import Monri

@objc public class MonriService : NSObject {
    
    private var creditCardNotValidMessage: String =  "credit_card_not_valid"
    private var tempCardId: String = ""
    private var cardProcessingIsDone: Bool = false
    private var cardProcessingIsDoneWithError: Bool = false
    private var cardProcessingErrorString: String = ""
    private var maskedCard: String = ""
    private var developmentMode: Bool = true
    
    func setCardProcessingDoneWithError(value: Bool){
        cardProcessingIsDoneWithError = value
    }
    
    func setCardProcessingErrorString(value: String){
        cardProcessingErrorString = value
    }
    
    func setCardProcessingDone(value: Bool){
        cardProcessingIsDone = value
    }
    
    func setTempCardId(value: String){
        tempCardId = value
    }
    
    func setMaskedCard(value: String){
        maskedCard = value
    }
    
    func setDevelopmentMode(value: Bool){
        developmentMode = value
    }
    
    @objc public func getTempCardId() -> String{
        return tempCardId
    }
    
    @objc public func isCardProcessingDone() -> Bool{
        return cardProcessingIsDone
    }
    
    @objc public func isCardProcessingDoneWithError() -> Bool{
        return cardProcessingIsDoneWithError
    }
    
    @objc public func getCardProcessingErrorString() -> String{
        return cardProcessingErrorString
    }
    
    @objc public func getMaskedCard() -> String{
        return maskedCard
    }
    
    @objc public func getDevelopmentMode() -> Bool{
        return developmentMode
    }
    
    @objc public func getTempCardIdFromService(cardNumber:String , cardExpMonth:Int, cardExpYear:Int , cardCVC:String , tokenizeCard:Bool, merchantKey:String , authenticityToken:String, developmentMode:Bool )
    {
        
        let storyBoard = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let yourVC = storyBoard.instantiateViewController(withIdentifier: "mainview") as UIViewController
        
        let monri: MonriApi = {
            return MonriApi(yourVC, options: MonriApiOptions(authenticityToken: authenticityToken, developmentMode: developmentMode));
        }()
        
        let date = Date()
        let formatter = ISO8601DateFormatter()
        if #available(iOS 11.0, *) {
            formatter.formatOptions.insert(.withFractionalSeconds)
        } else {
            // Fallback on earlier versions
        }
        let timestamp = formatter.string(from: date);
        let token = UUID.init().uuidString
        // merchantKey, token, timestamp
        let digest = "\(merchantKey)\(token)\(timestamp)".sha512
        let tokenRequest = TokenRequest(token: token, digest: digest, timestamp: timestamp)
        
        var  card = Card( number:cardNumber, cvc:cardCVC, expMonth:cardExpMonth, expYear:cardExpYear)
        card.tokenizePan = tokenizeCard
        
        if !card.validateCard() {
            setCardProcessingDoneWithError(value: true)
            setCardProcessingErrorString(value:creditCardNotValidMessage)
            setCardProcessingDone(value:true)
        }
        else{
            monri.createToken(tokenRequest, paymentMethod: card) {
                result in
                switch result {
                case .error(let error):
                    self.setCardProcessingDoneWithError(value:true)
                    self.setCardProcessingErrorString(value:error.localizedDescription)
                    self.setCardProcessingDone(value:true)
                    
                case .token(let token):
                    self.setTempCardId(value:token.id)
                    self.setCardProcessingDone(value:true)
                    self.setMaskedCard(value: (card.type?.stringValue())! + "-" + card.last4)
                }
            }
        }
    }
    
    @objc public func getTempCardIdFromServiceForSavedCard(cardNumberMask:String , cardCVC:String , merchantKey:String , authenticityToken:String, developmentMode:Bool )
    {
        
        let storyBoard = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let yourVC = storyBoard.instantiateViewController(withIdentifier: "mainview") as UIViewController
        
        let monri: MonriApi = {
            return MonriApi(yourVC, options: MonriApiOptions(authenticityToken: authenticityToken, developmentMode: developmentMode));
        }()
        
        let date = Date()
        let formatter = ISO8601DateFormatter()
        if #available(iOS 11.0, *) {
            formatter.formatOptions.insert(.withFractionalSeconds)
        } else {
            // Fallback on earlier versions
        }
        let timestamp = formatter.string(from: date);
        let token = UUID.init().uuidString
        // merchantKey, token, timestamp
        let digest = "\(merchantKey)\(token)\(timestamp)".sha512
        let tokenRequest = TokenRequest(token: token, digest: digest, timestamp: timestamp)

        let savedCard = SavedCard (panToken:cardNumberMask, cvc:cardCVC)

        monri.createToken(tokenRequest, paymentMethod: savedCard) {
            result in
            switch result {
            case .error(let error):
                self.setCardProcessingDoneWithError(value:true)
                self.setCardProcessingErrorString(value:error.localizedDescription)
                self.setCardProcessingDone(value:true)

            case .token(let token):
                self.setTempCardId(value:token.id)
                self.setCardProcessingDone(value:true)
            }
        }

    }
}

